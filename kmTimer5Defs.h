/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* kmTimer5Defs.h
*
*  **Created on**: 12/25/2023 7:14:54 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2023 Krzysztof Moskwa
*  kmTimer5Defs.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KM_TIMER_5_DEFS_H_
#define KM_TIMER_5_DEFS_H_

#ifdef KM_DOXYGEN
#define TIMSK5
#define COM5C0
#define KM_CPU_AVR_LAYOUT_100
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef TIMSK5

#include "../kmCommon/kmCommon.h"
#include "../kmCpu/kmCpuDefs.h"
#include "kmTimerDefs.h"

/// Defines bottom value for Timer5
#define KM_TIMER5_BOTTOM KM_TCC_BOTOM
/// Defines top value for Timer5
#define KM_TIMER5_MAX KM_TCC_TOP_3
#define KM_TIMER5_TOP_8_BIT KM_TCC_TOP_1
#define KM_TIMER5_TOP_9_BIT KM_TCC_TOP_5
#define KM_TIMER5_TOP_10_BIT KM_TCC_TOP_4
#define KM_TIMER5_TOP_16_BIT KM_TCC_TOP_3
/// Defines mid value for Timer5
#define KM_TIMER5_MID KM_TCC_MID_3

/// Clock select of Timer5 - Disabled
#define KM_TCC5_STOP		(KMC_ALL_BITS_CLEARED				)
/// Clock select of Timer5 - Prescaler 1 / 1
#define KM_TCC5_PRSC_1		(						_BV(CS50)	)
/// Clock select of Timer5 - Prescaler 1 / 8
#define KM_TCC5_PRSC_8		(			_BV(CS51)				)
/// Clock select of Timer5 - Prescaler 1 / 64
#define KM_TCC5_PRSC_64	(				_BV(CS51) |	_BV(CS50)	)
/// Clock select of Timer5 - Prescaler 1 / 256
#define KM_TCC5_PRSC_256	(_BV(CS52)							)
/// Clock select of Timer5 - Prescaler 1 / 1024
#define KM_TCC5_PRSC_1024	(_BV(CS52) |			_BV(CS50)	)
/// Clock select of Timer5 - Enabled on falling signal
#define KM_TCC5_EXT_T5_FAL	(_BV(CS52) |_BV(CS51)				)
/// Clock select of Timer5 - Enabled on rising signal
#define KM_TCC5_EXT_T5_RIS	(_BV(CS52) |_BV(CS51) |	_BV(CS50)	)
/// Prescaler mask for Timer5 (allows to clear prescaler)
#define KM_TCC5_CS_MASK		KM_TCC5_EXT_T5_RIS

/// Timer5 mode 0 (Reg.A) - Normal top value 0xFFFF
#define KM_TCC5_MODE_0_A	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 0 (Reg.B)
#define KM_TCC5_MODE_0_B	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 1 (Reg.A) - PWM, Phase Correct, 8-bit top value 0x00FF
#define KM_TCC5_MODE_1_A	(												_BV(WGM50)	)
/// Timer5 mode 1 (Reg.B)
#define KM_TCC5_MODE_1_B	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 2 (Reg.A) - PWM, Phase Correct, 9-bit top value 0x01FF
#define KM_TCC5_MODE_2_A	(								_BV(WGM51)					)
/// Timer5 mode 2 (Reg.B)
#define KM_TCC5_MODE_2_B	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 3 (Reg.A) - PWM, Phase Correct, 10-bit top value 0x03FF
#define KM_TCC5_MODE_3_A	(								_BV(WGM51) |	_BV(WGM50)	)
/// Timer5 mode 3 (Reg.B)
#define KM_TCC5_MODE_3_B	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 4 (Reg.A) - CTC with OCR3A as top
#define KM_TCC5_MODE_4_A	(KMC_ALL_BITS_CLEARED										)
/// Timer5 mode 4 (Reg.B)
#define KM_TCC5_MODE_4_B	(				_BV(WGM52)									)
/// Timer5 mode 5 (Reg.A) - Fast PWM, 8-bit top value 0x00FF
#define KM_TCC5_MODE_5_A	(												_BV(WGM50)	)
/// Timer5 mode 5 (Reg.B)
#define KM_TCC5_MODE_5_B	(				_BV(WGM52)									)
/// Timer5 mode 6 (Reg.A) - Fast PWM, 9-bit top value 0x01FF
#define KM_TCC5_MODE_6_A	(								_BV(WGM51)					)
/// Timer5 mode 5 (Reg.B)
#define KM_TCC5_MODE_6_B	(				_BV(WGM52)									)
/// Timer5 mode 7 (Reg.A) - Fast PWM, 10-bit top value 0x03FF
#define KM_TCC5_MODE_7_A	(								_BV(WGM51) |	_BV(WGM50)	)
/// Timer5 mode 7 (Reg.B)
#define KM_TCC5_MODE_7_B	(				_BV(WGM52)									)
/// Timer5 mode 8 (Reg.A) - PWM, Phase and Frequency Correct with ICR3 as top
#define KM_TCC5_MODE_8_A	(KMC_ALL_BITS_CLEARED									)
/// Timer5 mode 8 (Reg.B)
#define KM_TCC5_MODE_8_B	(_BV(WGM53)													)
/// Timer5 mode 9 (Reg.A) - PWM, Phase and Frequency Correct with OCR3A as top
#define KM_TCC5_MODE_9_A	(												_BV(WGM50)	)
/// Timer5 mode 9 (Reg.B)
#define KM_TCC5_MODE_9_B	(_BV(WGM53)													)
/// Timer5 mode 10 (Reg.A) - PWM, Phase Correct with ICR3 as top
#define KM_TCC5_MODE_A_A	(								_BV(WGM51)					)
/// Timer5 mode 10 (Reg.B)
#define KM_TCC5_MODE_A_B	(_BV(WGM53)													)
/// Timer5 mode 11 (Reg.A) - PWM, Phase Correct with OCR3A as top
#define KM_TCC5_MODE_B_A	(								_BV(WGM51) |	_BV(WGM50)	)
/// Timer5 mode 11 (Reg.B)
#define KM_TCC5_MODE_B_B	(_BV(WGM53)													)
/// Timer5 mode 12 (Reg.A) - CTC with ICR3 as top
#define KM_TCC5_MODE_C_A	(KMC_ALL_BITS_CLEARED									)
/// Timer5 mode 12 (Reg.B)
#define KM_TCC5_MODE_C_B	(_BV(WGM53) |	_BV(WGM52)									)
/// Timer5 mode 13 (Reg.A) - Reserved
#define KM_TCC5_MODE_D_A	(												_BV(WGM50)	)
/// Timer5 mode 13 (Reg.B)
#define KM_TCC5_MODE_D_B	(_BV(WGM53) |	_BV(WGM52)									)
/// Timer5 mode 14 (Reg.A) - Fast PWM with ICR3 as top
#define KM_TCC5_MODE_E_A	(								_BV(WGM51)					)
/// Timer5 mode 14 (Reg.B)
#define KM_TCC5_MODE_E_B	(_BV(WGM53) |	_BV(WGM52)									)
/// Timer5 mode 15 (Reg.A) - Fast PWM with OCR3A as top
#define KM_TCC5_MODE_F_A	(								_BV(WGM51) |	_BV(WGM50)	)
/// Timer5 mode 15 (Reg.B)
#define KM_TCC5_MODE_F_B	(_BV(WGM53) |	_BV(WGM52)									)
/// Mode  mask for Timer5 (reg. A) (helps to clear mode)
#define KM_TCC5_MODE_MASK_A	KM_TCC5_MODE_F_A
/// Mode  mask for Timer5 (reg. B)
#define KM_TCC5_MODE_MASK_B	KM_TCC5_MODE_F_B

/// Normal port operation for compare match
#define KM_TCC5_A_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1A on compare match
#define KM_TCC5_A_COMP_OUT_TOGGLE	(				_BV(COM5A0)	)
/// Clear OC1A on compare match (set output to low level)
#define KM_TCC5_A_COMP_OUT_CLEAR	(_BV(COM5A1)				)
/// Set OC1A on compare match (set output to high level)
#define KM_TCC5_A_COMP_OUT_SET		(_BV(COM5A1) |	_BV(COM5A0)	)
#define KM_TCC5_COMP_A_MASK			KM_TCC5_A_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC5_B_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC5_B_COMP_OUT_TOGGLE	(				_BV(COM5B0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC5_B_COMP_OUT_CLEAR	(_BV(COM5B1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC5_B_COMP_OUT_SET		(_BV(COM5B1) |	_BV(COM5B0)	)
#define KM_TCC5_COMP_B_MASK			KM_TCC5_B_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC5_C_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC5_C_COMP_OUT_TOGGLE	(				_BV(COM5C0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC5_C_COMP_OUT_CLEAR	(_BV(COM5C1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC5_C_COMP_OUT_SET		(_BV(COM5C1) |	_BV(COM5C0)	)
#define KM_TCC5_COMP_C_MASK			KM_TCC5_C_COMP_OUT_SET

/// Normal port operation for compare match, OC3A disconnected
#define KM_TCC5_A_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3A Disconnected @n
/// WGM02 = 1: Toggle OC3A on Compare Match
#define KM_TCC5_A_PWM_COMP_OUT_TOGGLE				(				_BV(COM5A0)	)
/// Fast PWM: Clear OC3A on Compare Match, set OC3A at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3A on Compare Match when up-counting. Set OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM5A1)				)
/// Fast PWM: Set OC3A on Compare Match, clear OC3A at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC0A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM5A1) |	_BV(COM5A0)	)

/// Normal port operation for compare match, OC3B disconnected
#define KM_TCC5_B_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3B Disconnected @n
/// WGM02 = 1: Toggle OC3B on Compare Match
#define KM_TCC5_B_PWM_COMP_OUT_TOGGLE				(				_BV(COM5B0)	)
/// Fast PWM: Clear OC3B on Compare Match, set OC3B at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3B on Compare Match when up-counting. Set OC3B on @n
/// Compare Match when down-counting.
#define KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM5B1)				)
/// Fast PWM: Set OC3B on Compare Match, clear OC3B at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM5B1) |	_BV(COM5B0)	)

/// Normal port operation for compare match, OC3Cdisconnected
#define KM_TCC5_C_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3C Disconnected @n
/// WGM02 = 1: Toggle OC3C on Compare Match
#define KM_TCC5_C_PWM_COMP_OUT_TOGGLE				(				_BV(COM5C0)	)
/// Fast PWM: Clear OC3C on Compare Match, set OC3c at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3C on Compare Match when up-counting. Set OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM5C1)				)
/// Fast PWM: Set OC3C on Compare Match, clear OC3C at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3C on Compare Match when up-counting. Clear OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM5C1) |	_BV(COM5C0)	)

#define KM_KM_TCC5_INT_MASK_REG TIMSK5

#ifdef KM_CPU_AVR_LAYOUT_100
/// Direction register o Timer5 PWM outputs
#define KM_TCC5_PWM_DDR DDRL
/// Port register of Timer5 PWM outputs
#define KM_TCC5_PWM_PORT PORTL
/// PWM pin A of Timer5
#define KM_TCC5_PWM_PIN_A PL3
/// PMW pin B of Timer5
#define KM_TCC5_PWM_PIN_B PL4
/// PMW pin C of Timer5
#define KM_TCC5_PWM_PIN_C PL5

/// Timer/Counter5 Input Capture Pin
#define KM_TCC5_ICP5_PIN PL1
/// Timer/Counter5 Input Capture DDR
#define KM_TCC5_ICP5_PORT_DDR DDRL
/// Timer/Counter5 Input Capture output port
#define KM_TCC5_ICP5_PORT_OUTPUT PORTL
/// Timer/Counter5 Input Capture input port
#define KM_TCC5_ICP5_PORT_INPUT PORTL

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC5_T5_PIN PL2
#endif /* AVR_CPU_LAYOUT_100 */

/// Byte value of PWM pin A of Timer5
#define KM_TCC5_PWM_BV_A	_BV(KM_TCC5_PWM_PIN_A)
/// Byte value of PWM pin B of Timer5
#define KM_TCC5_PWM_BV_B	_BV(KM_TCC5_PWM_PIN_B)
/// Byte value of PWM pin C of Timer5
#define KM_TCC5_PWM_BV_C	_BV(KM_TCC5_PWM_PIN_C)

#endif /* TIMSK5 */

#ifdef __cplusplus
}
#endif
#endif /* KM_TIMER_5_DEFS_H_ */

