/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief Common definitions for Timers.
* - kmTimerDefs.h
* - kmTimer0Defs.h
* - kmTimer1Defs.h
* - kmTimer2Defs.h
* - kmTimer3Defs.h
* - kmTimer4Defs.h
* - kmTimer5Defs.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmTimersCommon library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef KM_TIMERDEFS_H_
#define KM_TIMERDEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../kmCommon/kmCommon.h"
#include "kmTimer0Defs.h"
#include "kmTimer1Defs.h"
#include "kmTimer2Defs.h"
#include "kmTimer3Defs.h"
#include "kmTimer4Defs.h"
#include "kmTimer5Defs.h"

/// Timers top value (255)
#define KM_TCC_TOP_1		0xFFu
/// Timers top value (4095)
#define KM_TCC_TOP_2		0x0FFFu
/// Timers top value (65535)
#define KM_TCC_TOP_3		0xFFFFu
/// Timers top value (1023)
#define KM_TCC_TOP_4		0x03FFu
/// Timers top value (511)
#define KM_TCC_TOP_5		0x01FFu

/// Timers top value (127)
#define KM_TCC_MID_1		0x7Fu
/// Timers top value (2047)
#define KM_TCC_MID_2		0x07FFu
/// Timers top value (32767)
#define KM_TCC_MID_3		0x7FFFu
/// Timers top value (511)
#define KM_TCC_MID_4		0x01FFu
/// Timers top value (255)
#define KM_TCC_MID_5		0x00FFu

/// Timers bottom value
#define KM_TCC_BOTOM		0x00u

#ifdef __cplusplus
}
#endif
#endif /* KM_TIMERDEFS_H_ */
