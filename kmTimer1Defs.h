/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @brief Common definitions for Timers.
* kmTimer1Defs.h
*
*  **Created on**: Dec 10, 2023 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmTimersCommon library for AVR MCUs @n
*  **Copyright (C) 2023  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KM_TIMER_1_DEFS_H_
#define KM_TIMER_1_DEFS_H_

#ifdef KM_DOXYGEN
#define COM1C0
#define KM_CPU_AVR_LAYOUT_100
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif

#include "../kmCommon/kmCommon.h"
#include "../kmCpu/kmCpuDefs.h"
#include "../kmTimersCommon/kmTimerDefs.h"

/// Defines bottom value for Timer1
#define KM_TIMER1_BOTTOM KM_TCC_BOTOM
/// Defines top value for Timer1
#define KM_TIMER1_MAX KM_TCC_TOP_3
#define KM_TIMER1_TOP_8_BIT KM_TCC_TOP_1
#define KM_TIMER1_TOP_9_BIT KM_TCC_TOP_5
#define KM_TIMER1_TOP_10_BIT KM_TCC_TOP_4
#define KM_TIMER1_TOP_16_BIT KM_TCC_TOP_3
/// Defines mid value for Timer1
#define KM_TIMER1_MID KM_TCC_MID_3


#ifdef TICIE1
#define ICIE1 TICIE1
#endif

#ifdef TIMSK
#define TIMSK1 TIMSK
#endif

/// Clock select of Timer1 - Disabled
#define KM_TCC1_STOP		(KMC_ALL_BITS_CLEARED				)
/// Clock select of Timer1 - Prescaler 1 / 1
#define KM_TCC1_PRSC_1		(						_BV(CS10)	)
/// Clock select of Timer1 - Prescaler 1 / 8
#define KM_TCC1_PRSC_8		(			_BV(CS11)				)
/// Clock select of Timer1 - Prescaler 1 / 64
#define KM_TCC1_PRSC_64	(				_BV(CS11) |	_BV(CS10)	)
/// Clock select of Timer1 - Prescaler 1 / 256
#define KM_TCC1_PRSC_256	(_BV(CS12)							)
/// Clock select of Timer1 - Prescaler 1 / 1024
#define KM_TCC1_PRSC_1024	(_BV(CS12) |			_BV(CS10)	)
/// Clock select of Timer1 - Enabled on falling signal
#define KM_TCC1_EXT_T1_FAL	(_BV(CS12) |_BV(CS11)				)
/// Clock select of Timer1 - Enabled on rising signal
#define KM_TCC1_EXT_T1_RIS	(_BV(CS12) |_BV(CS11) |	_BV(CS10)	)
/// Prescaler mask for Timer1 (allows to clear prescaler)
#define KM_TCC1_CS_MASK		KM_TCC1_EXT_T1_RIS

/// Timer1 mode 0 (Reg.A) - Normal top value 0xFFFF
#define KM_TCC1_MODE_0_A	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 0 (Reg.B)
#define KM_TCC1_MODE_0_B	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 1 (Reg.A) - PWM, Phase Correct, 8-bit top value 0x00FF
#define KM_TCC1_MODE_1_A	(												_BV(WGM10)	)
/// Timer1 mode 1 (Reg.B)
#define KM_TCC1_MODE_1_B	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 2 (Reg.A) - PWM, Phase Correct, 9-bit top value 0x01FF
#define KM_TCC1_MODE_2_A	(								_BV(WGM11)					)
/// Timer1 mode 2 (Reg.B)
#define KM_TCC1_MODE_2_B	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 3 (Reg.A) - PWM, Phase Correct, 10-bit top value 0x03FF
#define KM_TCC1_MODE_3_A	(								_BV(WGM11) |	_BV(WGM10)	)
/// Timer1 mode 3 (Reg.B)
#define KM_TCC1_MODE_3_B	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 4 (Reg.A) - CTC with OCR1A as top
#define KM_TCC1_MODE_4_A	(KMC_ALL_BITS_CLEARED										)
/// Timer1 mode 4 (Reg.B)
#define KM_TCC1_MODE_4_B	(				_BV(WGM12)									)
/// Timer1 mode 5 (Reg.A) - Fast PWM, 8-bit top value 0x00FF
#define KM_TCC1_MODE_5_A	(												_BV(WGM10)	)
/// Timer1 mode 5 (Reg.B)
#define KM_TCC1_MODE_5_B	(				_BV(WGM12)									)
/// Timer1 mode 6 (Reg.A) - Fast PWM, 9-bit top value 0x01FF
#define KM_TCC1_MODE_6_A	(								_BV(WGM11)					)
/// Timer1 mode 5 (Reg.B)
#define KM_TCC1_MODE_6_B	(				_BV(WGM12)									)
/// Timer1 mode 7 (Reg.A) - Fast PWM, 10-bit top value 0x03FF
#define KM_TCC1_MODE_7_A	(								_BV(WGM11) |	_BV(WGM10)	)
/// Timer1 mode 7 (Reg.B)
#define KM_TCC1_MODE_7_B	(				_BV(WGM12)									)
/// Timer1 mode 8 (Reg.A) - PWM, Phase and Frequency Correct with ICR1 as top
#define KM_TCC1_MODE_8_A	(KMC_ALL_BITS_CLEARED									)
/// Timer1 mode 8 (Reg.B)
#define KM_TCC1_MODE_8_B	(_BV(WGM13)													)
/// Timer1 mode 9 (Reg.A) - PWM, Phase and Frequency Correct with OCR1A as top
#define KM_TCC1_MODE_9_A	(												_BV(WGM10)	)
/// Timer1 mode 9 (Reg.B)
#define KM_TCC1_MODE_9_B	(_BV(WGM13)													)
/// Timer1 mode 10 (Reg.A) - PWM, Phase Correct with ICR1 as top
#define KM_TCC1_MODE_A_A	(								_BV(WGM11)					)
/// Timer1 mode 10 (Reg.B)
#define KM_TCC1_MODE_A_B	(_BV(WGM13)													)
/// Timer1 mode 11 (Reg.A) - PWM, Phase Correct with OCR1A as top
#define KM_TCC1_MODE_B_A	(								_BV(WGM11) |	_BV(WGM10)	)
/// Timer1 mode 11 (Reg.B)
#define KM_TCC1_MODE_B_B	(_BV(WGM13)													)
/// Timer1 mode 12 (Reg.A) - CTC with ICR1 as top
#define KM_TCC1_MODE_C_A	(KMC_ALL_BITS_CLEARED									)
/// Timer1 mode 12 (Reg.B)
#define KM_TCC1_MODE_C_B	(_BV(WGM13) |	_BV(WGM12)									)
/// Timer1 mode 13 (Reg.A) - Reserved
#define KM_TCC1_MODE_D_A	(												_BV(WGM10)	)
/// Timer1 mode 13 (Reg.B)
#define KM_TCC1_MODE_D_B	(_BV(WGM13) |	_BV(WGM12)									)
/// Timer1 mode 14 (Reg.A) - Fast PWM with ICR1 as top
#define KM_TCC1_MODE_E_A	(								_BV(WGM11)					)
/// Timer1 mode 14 (Reg.B)
#define KM_TCC1_MODE_E_B	(_BV(WGM13) |	_BV(WGM12)									)
/// Timer1 mode 15 (Reg.A) - Fast PWM with OCR1A as top
#define KM_TCC1_MODE_F_A	(								_BV(WGM11) |	_BV(WGM10)	)
/// Timer1 mode 15 (Reg.B)
#define KM_TCC1_MODE_F_B	(_BV(WGM13) |	_BV(WGM12)									)
/// Mode  mask for Timer1 (reg. A) (helps to clear mode)
#define KM_TCC1_MODE_MASK_A	KM_TCC1_MODE_F_A
/// Mode  mask for Timer1 (reg. B)
#define KM_TCC1_MODE_MASK_B	KM_TCC1_MODE_F_B

/// Normal port operation for compare match
#define KM_TCC1_A_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1A on compare match
#define KM_TCC1_A_COMP_OUT_TOGGLE	(				_BV(COM1A0)	)
/// Clear OC1A on compare match (set output to low level)
#define KM_TCC1_A_COMP_OUT_CLEAR	(_BV(COM1A1)				)
/// Set OC1A on compare match (set output to high level)
#define KM_TCC1_A_COMP_OUT_SET		(_BV(COM1A1) |	_BV(COM1A0)	)
#define KM_TCC1_COMP_A_MASK			KM_TCC1_A_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC1_B_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC1_B_COMP_OUT_TOGGLE	(				_BV(COM1B0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC1_B_COMP_OUT_CLEAR	(_BV(COM1B1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC1_B_COMP_OUT_SET		(_BV(COM1B1) |	_BV(COM1B0)	)
#define KM_TCC1_COMP_B_MASK			KM_TCC1_B_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC1_C_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC1_C_COMP_OUT_TOGGLE	(				_BV(COM1C0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC1_C_COMP_OUT_CLEAR	(_BV(COM1C1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC1_C_COMP_OUT_SET		(_BV(COM1C1) |	_BV(COM1C0)	)
#define KM_TCC1_COMP_C_MASK			KM_TCC1_C_COMP_OUT_SET

/// Normal port operation for compare match, OC1A disconnected
#define KM_TCC1_A_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC1A Disconnected @n
/// WGM02 = 1: Toggle OC1A on Compare Match
#define KM_TCC1_A_PWM_COMP_OUT_TOGGLE				(				_BV(COM1A0)	)
/// Fast PWM: Clear OC1A on Compare Match, set OC1A at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC1A on Compare Match when up-counting. Set OC1A on @n
/// Compare Match when down-counting.
#define KM_TCC1_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM1A1)				)
/// Fast PWM: Set OC1A on Compare Match, clear OC1A at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC0A on Compare Match when up-counting. Clear OC1A on @n
/// Compare Match when down-counting.
#define KM_TCC1_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM1A1) |	_BV(COM1A0)	)

/// Normal port operation for compare match, OC1B disconnected
#define KM_TCC1_B_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC1B Disconnected @n
/// WGM02 = 1: Toggle OC1B on Compare Match
#define KM_TCC1_B_PWM_COMP_OUT_TOGGLE				(				_BV(COM1B0)	)
/// Fast PWM: Clear OC1B on Compare Match, set OC1B at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC1B on Compare Match when up-counting. Set OC1B on @n
/// Compare Match when down-counting.
#define KM_TCC1_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM1B1)				)
/// Fast PWM: Set OC1B on Compare Match, clear OC1B at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC1A on Compare Match when up-counting. Clear OC1A on @n
/// Compare Match when down-counting.
#define KM_TCC1_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM1B1) |	_BV(COM1B0)	)

#ifdef COM1C0
/// Normal port operation for compare match, OC1Cdisconnected
#define KM_TCC1_C_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC1C Disconnected @n
/// WGM02 = 1: Toggle OC1C on Compare Match
#define KM_TCC1_C_PWM_COMP_OUT_TOGGLE				(				_BV(COM1C0)	)
/// Fast PWM: Clear OC1C on Compare Match, set OC1c at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC1C on Compare Match when up-counting. Set OC1C on @n
/// Compare Match when down-counting.
#define KM_TCC1_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM1C1)				)
/// Fast PWM: Set OC1C on Compare Match, clear OC1C at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC1C on Compare Match when up-counting. Clear OC1C on @n
/// Compare Match when down-counting.
#define KM_TCC1_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM1C1) |	_BV(COM1C0)	)
#endif /* COM1C0 */

#define KM_TCC1_INT_MASK_REG TIMSK1

#if defined(KM_CPU_AVR_LAYOUT_28) // ATmega8/16/328
/// Direction register o Timer1 PWM outputs
#define KM_TCC1_PWM_DDR DDRB
/// Port register of Timer1 PWM outputs
#define KM_TCC1_PWM_PORT PORTB
/// PWM pin A of Timer1
#define KM_TCC1_PWM_PIN_A PB1
/// PMW pin B of Timer1
#define KM_TCC1_PWM_PIN_B PB2

/// Timer/Counter1 Input Capture Pin
#define KM_TCC1_ICP1_PIN PB0
/// Timer/Counter1 Input Capture DDR
#define KM_TCC1_ICP1_PORT_DDR DDRB
/// Timer/Counter1 Input Capture output port
#define KM_TCC1_ICP1_PORT_OUTPUT PORTB
/// Timer/Counter3 Input Capture input port
#define KM_TCC1_ICP1_PORT_INPUT PINB

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC1_T1_PIN PD5
#endif

#if defined(KM_CPU_AVR_LAYOUT_40) // ATmega32/644/1284
/// Direction register o Timer1 PWM outputs
#define KM_TCC1_PWM_DDR DDRD
/// Port register of Timer1 PWM outputs
#define KM_TCC1_PWM_PORT PORTD
/// PWM pin A of Timer1
#define KM_TCC1_PWM_PIN_A PD5
/// PMW pin B of Timer1
#define KM_TCC1_PWM_PIN_B PD4

/// Timer/Counter1 Input Capture Pin
#define KM_TCC1_ICP1_PIN PD6
/// Timer/Counter1 Input Capture DDR
#define KM_TCC1_ICP1_PORT_DDR DDRD
/// Timer/Counter1 Input Capture output port
#define KM_TCC1_ICP1_PORT_OUTPUT PORTD
/// Timer/Counter3 Input Capture input port
#define KM_TCC1_ICP1_PORT_INPUT PIND

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC1_T1_PIN PB1
#endif

#ifdef KM_CPU_AVR_LAYOUT_100
/// Direction register o Timer1 PWM outputs
#define KM_TCC1_PWM_DDR DDRB
/// Port register of Timer1 PWM outputs
#define KM_TCC1_PWM_PORT PORTB
/// PWM pin A of Timer1
#define KM_TCC1_PWM_PIN_A PB5
/// PMW pin B of Timer1
#define KM_TCC1_PWM_PIN_B PB6
/// PMW pin C of Timer1
#define KM_TCC1_PWM_PIN_C PB7

/// Timer/Counter1 Input Capture Pin
#define KM_TCC1_ICP1_PIN PD4
/// Timer/Counter1 Input Capture DDR
#define KM_TCC1_ICP1_PORT_DDR DDRD
/// Timer/Counter1 Input Capture output port
#define KM_TCC1_ICP1_PORT_OUTPUT PORTD
/// Timer/Counter3 Input Capture input port
#define KM_TCC1_ICP1_PORT_INPUT PIND

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC1_T1_PIN PE6
#endif /* KM_CPU_AVR_LAYOUT_100 */

/// Byte value of PWM pin A of Timer1
#define KM_TCC1_PWM_BV_A	_BV(KM_TCC1_PWM_PIN_A)
/// Byte value of PWM pin B of Timer1
#define KM_TCC1_PWM_BV_B	_BV(KM_TCC1_PWM_PIN_B)
/// Byte value of PWM pin C of Timer1
#define KM_TCC1_PWM_BV_C	_BV(KM_TCC1_PWM_PIN_C)

#endif
#ifdef __cplusplus
}

#endif /* KM_TIMER_1_DEFS_H_ */
