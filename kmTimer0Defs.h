/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @brief Common definitions for Timers.
* kmTimer0Defs.h
*
*  **Created on**: Aug 10, 2019 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmTimersCommon library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KM_TIMER0DEFS_H_
#define KM_TIMER0DEFS_H_

#ifdef KM_DOXYGEN
#ifdef OCR0
#undef OCR0
#endif /* OCR0 */
#define KM_CPU_AVR_LAYOUT_100
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "../kmCommon/kmCommon.h"
#include "../kmCpu/kmCpuDefs.h"

/// Defines bottom value for Timer0
#define KM_TIMER0_BOTTOM KM_TCC_BOTOM
/// Defines top value for Timer0
#define KM_TIMER0_MAX KM_TCC_TOP_1
/// Defines mid value for Timer0
#define KM_TIMER0_MID KM_TCC_MID_1

#ifdef OCR0
// @internal
#define OCR0A OCR0
// @internal
#define TCCR0A TCCR0
// @internal
#define TCCR0B TCCR0
// @internal
#define COM0A0 COM00
// @internal
#define COM0A1 COM01
// @internal
#define OCIE0A OCIE0
// @internal
#define TIMER0_COMPA_vect TIMER0_COMP_vect
/** Aux definition allowing to use common code base for old and new AVR MCUs. 
When this is defined, then OCB and Timer modes 5-7 are not available
*/
#endif

#ifdef TIMSK
#define TIMSK0 TIMSK
#endif

// Definitions for TCCR0B - Timer/Counter Control Register B @n
/// Clock select of Timer0 - Disabled
#define KM_TCC0_STOP		(KMC_ALL_BITS_CLEARED			)
/// Clock select of Timer0 - Prescaler 1 / 1
#define KM_TCC0_PRSC_1		(						_BV(CS00))
/// Clock select of Timer0 - Prescaler 1 / 8			
#define KM_TCC0_PRSC_8		(			_BV(CS01)			)
/// Clock select of Timer0 - Prescaler 1 / 64
#define KM_TCC0_PRSC_64	(			_BV(CS01) |	_BV(CS00))
/// Clock select of Timer0 - Prescaler 1 / 256
#define KM_TCC0_PRSC_256	(_BV(CS02)						)
/// Clock select of Timer0 - Prescaler 1 / 1024
#define KM_TCC0_PRSC_1024	(_BV(CS02) |			_BV(CS00))
/// Clock select of Timer0 - Enabled on falling signal on T0
#define KM_TCC0_EXT_T0_FAL	(_BV(CS02) |_BV(CS01)			)
/// Clock select of Timer0 - Enabled on rising signal on T0
#define KM_TCC0_EXT_T0_RIS	(_BV(CS02) |_BV(CS01) |	_BV(CS00))
/// Prescaler mask for Timer0 (helps to clear prescaler)
#define KM_TCC0_CS_MASK		KM_TCC0_EXT_T0_RIS

// Definitions for TCCR0A & TCCR0B - Timer/Counter Control Registers A&B
/// Timer0 mode 0 (reg. A) - Normal top value 0xFF
#define KM_TCC0_MODE_0_A	(KMC_ALL_BITS_CLEARED										)
/// Timer0 mode 0 (reg. B) 
#define KM_TCC0_MODE_0_B	(KMC_ALL_BITS_CLEARED										)
/// Timer0 mode 1 (reg. A) - PWM, Phase Correct, 8-bit top value 0xFF
#define KM_TCC0_MODE_1_A	(												_BV(WGM00)	)
/// Timer0 mode 1 (reg. B)
#define KM_TCC0_MODE_1_B	(KMC_ALL_BITS_CLEARED										)
/// Timer0 mode 2 (reg. A) - CTC with OCR0A as top
#define KM_TCC0_MODE_2_A	(								_BV(WGM01)					)
/// Timer0 mode 2 (reg. B)
#define KM_TCC0_MODE_2_B	(KMC_ALL_BITS_CLEARED										)
/// Timer0 mode 3 (reg. A) - Fast PWM, top value 0xFF
#define KM_TCC0_MODE_3_A	(								_BV(WGM01) |	_BV(WGM00)	)
/// Timer0 mode 3 (reg. B)
#define KM_TCC0_MODE_3_B	(KMC_ALL_BITS_CLEARED										)
/// Timer0 mode 4 - RESERVED
#define KM_TCC0_MODE_4_RESERVED	(KMC_ALL_BITS_CLEARED									)

#ifndef OCR0
/// Timer0 mode 5 (reg. A) - PWM, Phase Correct with OCR0A as top
#define KM_TCC0_MODE_5_A	(												_BV(WGM00)	)
/// Timer0 mode 5 (reg. B)
#define KM_TCC0_MODE_5_B	(				_BV(WGM02)									)
/// Timer0 mode 6 - RESERVED
#define KM_TCC0_MODE_6_RESERVED	(KMC_ALL_BITS_CLEARED									)
/// Timer0 mode 7 (reg. A) - Fast PWM with OCR0A as top
#define KM_TCC0_MODE_7_A	(								_BV(WGM01) |	_BV(WGM00)	)
/// Timer0 mode 7 (reg. B)
#define KM_TCC0_MODE_7_B	(				_BV(WGM02)									)
/// Mode  mask for Timer0 (reg. A) (helps to clear mode)
#define KM_TCC0_MODE_MASK_A	KM_TCC0_MODE_7_A
/// Mode  mask for Timer0 (reg. B)
#define KM_TCC0_MODE_MASK_B	KM_TCC0_MODE_7_B
#else
/// Mode  mask for Timer0 (reg. A) (helps to clear mode)
#define KM_TCC0_MODE_MASK_A	KM_TCC0_MODE_3_A
#endif

// Definitions for TCCR0A - Timer/Counter Control Register A
/// Normal port operation for compare match
#define KM_TCC0_A_COMP_OUT_NONE						(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1A on compare match
#define KM_TCC0_A_COMP_OUT_TOGGLE					(				_BV(COM0A0)	)
/// Clear OC1A on compare match (set output to low level)
#define KM_TCC0_A_COMP_OUT_CLEAR					(_BV(COM0A1)				)
/// Set OC1A on compare match (set output to high level)
#define KM_TCC0_A_COMP_OUT_SET						(_BV(COM0A1) |	_BV(COM0A0)	)
/// COM_A mask for Timer0 (helps to clear COM_A)
#define KM_TCC0_COMA_MASK			KM_TCC0_A_COMP_OUT_SET

/// Normal port operation for compare match, OC0A disconnected
#define KM_TCC0_A_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC0A Disconnected @n
/// WGM02 = 1: Toggle OC0A on Compare Match
#define KM_TCC0_A_PWM_COMP_OUT_TOGGLE				(				_BV(COM0A0)	)
/// Fast PWM: Clear OC0A on Compare Match, set OC0A at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC0A on Compare Match when up-counting. Set OC0A on @n
/// Compare Match when down-counting.
#define KM_TCC0_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM0A1)				)
/// Fast PWM: Set OC0A on Compare Match, clear OC0A at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC0A on Compare Match when up-counting. Clear OC0A on @n
/// Compare Match when down-counting.
#define KM_TCC0_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM0A1) |	_BV(COM0A0)	)

#ifndef OCR0
/// Normal port operation for compare match
#define KM_TCC0_B_COMP_OUT_NONE						(KMC_ALL_BITS_CLEARED		)
/// Toggle OC0B on compare match
#define KM_TCC0_B_COMP_OUT_TOGGLE					(				_BV(COM0B0)	)
/// Clear OC0B on compare match (set output to low level)
#define KM_TCC0_B_COMP_OUT_CLEAR					(_BV(COM0B1)				)
/// Set OC0B on compare match (set output to high level)
#define KM_TCC0_B_COMP_OUT_SET						(_BV(COM0B1) |	_BV(COM0B0)	)
/// COM_B mask for Timer0 (helps to clear COM_B)
#define KM_TCC0_COMB_MASK			KM_TCC0_B_COMP_OUT_SET

/// Normal port operation for compare match, OC0B disconnected
#define KM_TCC0_B_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Reserver
#define KM_TCC0_B_PWM_COMP_OUT_RESERVED				(				_BV(COM0B0)	)
/// Fast PWM: Clear OC0B on Compare Match, set OC0B at BOTTOM (non-inverting mode)  @n
/// Phase Accurate PWM: Clear OC0B on Compare Match when up-counting. Set OC0B on  @n
/// Compare Match when down-counting.
#define KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM0B1)				)
/// Fast PWM: Set OC0B on Compare Match, clear OC0B at BOTTOM (inverting mode)  @n
/// Phase Accurate PWM: Set OC0B on Compare Match when up-counting. Clear OC0B on  @n
/// Compare Match when down-counting.
#define KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM0B1) |	_BV(COM0B0)	)
#endif

#ifndef TIMSK
/// Universal definition of Interrupt Mask Register allowing to use common code base for old AVR MCUs.
#define KM_TCC0_INT_MASK_REG TIMSK1
#else
/// Universal definition of Interrupt Mask Register allowing to use common code base for old AVR MCUs.
#define KM_TCC0_INT_MASK_REG TIMSK
#endif

#if defined(KM_CPU_AVR_LAYOUT_28) // ATmega8/16/328
/// Direction register o Timer0 PWM outputs
#define KM_TCC0_PWM_DDR DDRD
/// Port register of Timer0 PWM outputs
#define KM_TCC0_PWM_PORT PORTD
/// PWM pin A of Timer0
#define KM_TCC0_PWM_PIN_A PD6
/// PMW pin B of Timer0
#define KM_TCC0_PWM_PIN_B PD5
///
#define KM_TCC0_T0_PIN PD4
#endif

#if defined(KM_CPU_AVR_LAYOUT_40) //ATmega32/644
/// Direction register o Timer0 PWM outputs
#define KM_TCC0_PWM_DDR DDRB
/// Port register of Timer0 PWM outputs
#define KM_TCC0_PWM_PORT PORTB
/// PWM pin A of Timer0
#define KM_TCC0_PWM_PIN_A PB3
#ifndef OCR0
/// PMW pin B of Timer0
#define KM_TCC0_PWM_PIN_B PB4
#endif /* OCR0 */
/// Timer/Counter1 Input Capture Pin
#define KM_TCC0_T0_PIN PB0
#endif

#if defined(KM_CPU_AVR_LAYOUT_64) // ATmega256RFR2
/// Direction register o Timer0 PWM outputs
#define KM_TCC0_PWM_DDR DDRB
#define KM_TCC0_PWM_DDR_OCB DDRG
/// Port register of Timer0 PWM outputs
#define KM_TCC0_PWM_PORT PORTB
#define KM_TCC0_PWM_PORT_OCB PORTG
/// PWM pin A of Timer0
#define KM_TCC0_PWM_PIN_A PB7
/// PMW pin B of Timer0
#define KM_TCC0_PWM_PIN_B PG5
/// Timer/Counter0 Input Capture Pin
#define KM_TCC0_ICP1_PIN PD4
/// Timer/Counter0 Input Capture Pin
#define KM_TCC0_T0_PIN PD7
#endif /* KM_CPU_AVR_LAYOUT_64 */

#if defined(KM_CPU_AVR_LAYOUT_100) // ATmega256
/// Direction register o Timer0 PWM outputs
#define KM_TCC0_PWM_DDR DDRB
#define KM_TCC0_PWM_DDR_OCB DDRG
/// Port register of Timer0 PWM outputs
#define KM_TCC0_PWM_PORT PORTB
#define KM_TCC0_PWM_PORT_OCB PORTG
/// PWM pin A of Timer0
#define KM_TCC0_PWM_PIN_A PB7
/// PMW pin B of Timer0
#define KM_TCC0_PWM_PIN_B PG5
/// Timer/Counter0 Input Capture Pin
#define KM_TCC0_T0_PIN PD7
#endif /* KM_CPU_AVR_LAYOUT_100 */

/// Byte value of PWM pin A of Timer0
#define KM_TCC0_PWM_BV_A	_BV(KM_TCC0_PWM_PIN_A)
/// Byte value of PWM pin B of Timer0
#define KM_TCC0_PWM_BV_B	_BV(KM_TCC0_PWM_PIN_B)

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* KM_TIME0RDEFS_H_ */
