# ReadMe
# kmTimersCommon library for AVR MCUs

This AVR MCU library provides common definitions and configurations for up to 6 hardware timers (depends on the MCU capability), essential for precise timing and PWM generation. It offers seamless integration with AVR microcontrollers, allowing developers to efficiently manage timer functionalities.

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-05-25)

## Overview
This library, designed for AVR MCUs, facilitates timer setup and operation, crucial for various tasks like timekeeping, event triggering, and PWM generation. It offers a comprehensive set of definitions and configurations for up to 6 hardware timers (depends on the MCU capability), enabling developers to optimize timing-related tasks efficiently.

## Dependencies
- [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)

## Usage
Getting this library and adding it to own project:
- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmTimersCommon.git kmTimersCommon
```
- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

- Include the library headers in your AVR MCU project.
- Configure the desired timer(s) by setting appropriate modes and prescaler values.
- Optionally, set up output compare registers (OCR) for PWM generation or input capture for precise event timing.
- Enable timer interrupts if needed for tasks such as overflow detection or compare match events.
- Implement your application logic within the main loop, utilizing the timer functionality as required.

## Example Code
```c
#include "kmTimerDefs.h"

int main() {
    // Configure Timer0 for PWM generation
    // Set mode to Fast PWM with OCR0A as top
    TCCR0A |= KM_TCC0_MODE_7_A;
    TCCR0B |= KM_TCC0_PRSC_256; // Set prescaler to 256
    OCR0A = 128; // Set PWM duty cycle

    // Enable Timer0 overflow interrupt
    TIMSK0 |= (1 << TOIE0);

    // Main loop
    while (1) {
        // Your application logic here
    }

    return 0;
}
```

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)