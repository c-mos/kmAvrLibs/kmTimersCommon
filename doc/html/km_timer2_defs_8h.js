var km_timer2_defs_8h =
[
    [ "KM_TCC2_COMP_OUT_CLEAR", "km_timer2_defs_8h.html#ac0b762708118404ce569496a54877539", null ],
    [ "KM_TCC2_COMP_OUT_NORMAL", "km_timer2_defs_8h.html#ab54d2b8de3fa5b71b0170598d7cf66e0", null ],
    [ "KM_TCC2_COMP_OUT_SET", "km_timer2_defs_8h.html#a06bb8ce6f2c725f7f5d44f44a972ad3f", null ],
    [ "KM_TCC2_COMP_OUT_TOGGLE", "km_timer2_defs_8h.html#af3ea862ef2dff9922392c23ad25b0a9a", null ],
    [ "KM_TCC2_CS_MASK", "km_timer2_defs_8h.html#a4cdfb45a5c930decd15af03f662ee523", null ],
    [ "KM_TCC2_PRSC_1", "km_timer2_defs_8h.html#a2895ccc6a644436ef170b12084772ffc", null ],
    [ "KM_TCC2_PRSC_1024", "km_timer2_defs_8h.html#a7e3be548c66ac52430a1789a6389265b", null ],
    [ "KM_TCC2_PRSC_128", "km_timer2_defs_8h.html#aef23557195b788cbd0d43db01ee66da4", null ],
    [ "KM_TCC2_PRSC_256", "km_timer2_defs_8h.html#ab93c30acd4e1f9b317c1852826d77f40", null ],
    [ "KM_TCC2_PRSC_32", "km_timer2_defs_8h.html#a2b9df66319dba38ba07ff2a9d879e971", null ],
    [ "KM_TCC2_PRSC_64", "km_timer2_defs_8h.html#ab9dc17f7bde1878dcc5d0cc4d9830572", null ],
    [ "KM_TCC2_PRSC_8", "km_timer2_defs_8h.html#a2d2c8839885c3a7b13c9e58d7f62f272", null ],
    [ "KM_TCC2_PWM_COMP_OUT_CLEAR_UP_SET_DOWN", "km_timer2_defs_8h.html#a7d63cdf6821884e77d0260f6f73caee3", null ],
    [ "KM_TCC2_PWM_COMP_OUT_NORMAL", "km_timer2_defs_8h.html#a8cb0814a83944df85f13b774142c73ff", null ],
    [ "KM_TCC2_PWM_COMP_OUT_RESERVED", "km_timer2_defs_8h.html#a8d72545abe76ed3484ce50bdf452b130", null ],
    [ "KM_TCC2_PWM_COMP_OUT_SET_UP_CLEAR_DOWN", "km_timer2_defs_8h.html#a206849ef6b4cfc71a30137d23acbe512", null ],
    [ "KM_TCC2_STOP", "km_timer2_defs_8h.html#a0c1c19efddc89ecab5c0f5d11feb9691", null ],
    [ "KM_TCC_2_MODE_0", "km_timer2_defs_8h.html#afe62258413bd43e149d1140a1de9a636", null ],
    [ "KM_TCC_2_MODE_1", "km_timer2_defs_8h.html#acfa1df52898f7f2a53738ad907e91dce", null ],
    [ "KM_TCC_2_MODE_2", "km_timer2_defs_8h.html#aaf02034da81eb72ab167419b18ae2934", null ],
    [ "KM_TCC_2_MODE_3", "km_timer2_defs_8h.html#a4406282d514be85d1dd796b0474ec198", null ]
];