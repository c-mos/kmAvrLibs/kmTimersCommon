/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @brief Common definitions for Timers.
* kmTimer3Defs.h
*
*  **Created on**: Dec 10, 2023 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmTimersCommon library for AVR MCUs @n
*  **Copyright (C) 2023  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef KM_TIMER_3_DEFS_H_
#define KM_TIMER_3_DEFS_H_

#ifdef KM_DOXYGEN
#define TIMSK3
#define COM3C0
#define KM_CPU_AVR_LAYOUT_100
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef TIMSK3

#include "../kmCommon/kmCommon.h"
#include "../kmCpu/kmCpuDefs.h"

/// Defines bottom value for Timer3
#define KM_TIMER3_BOTTOM KM_TCC_BOTOM
/// Defines top value for Timer3
#define KM_TIMER3_MAX KM_TCC_TOP_3
#define KM_TIMER3_TOP_8_BIT KM_TCC_TOP_1
#define KM_TIMER3_TOP_9_BIT KM_TCC_TOP_5
#define KM_TIMER3_TOP_10_BIT KM_TCC_TOP_4
#define KM_TIMER3_TOP_16_BIT KM_TCC_TOP_3
/// Defines mid value for Timer3
#define KM_TIMER3_MID KM_TCC_MID_3

/// Clock select of Timer3 - Disabled
#define KM_TCC3_STOP		(KMC_ALL_BITS_CLEARED				)
/// Clock select of Timer3 - Prescaler 1 / 1
#define KM_TCC3_PRSC_1		(						_BV(CS30)	)
/// Clock select of Timer3 - Prescaler 1 / 8
#define KM_TCC3_PRSC_8		(			_BV(CS31)				)
/// Clock select of Timer3 - Prescaler 1 / 64
#define KM_TCC3_PRSC_64	(				_BV(CS31) |	_BV(CS30)	)
/// Clock select of Timer3 - Prescaler 1 / 256
#define KM_TCC3_PRSC_256	(_BV(CS32)							)
/// Clock select of Timer3 - Prescaler 1 / 1024
#define KM_TCC3_PRSC_1024	(_BV(CS32) |			_BV(CS30)	)
/// Clock select of Timer3 - Enabled on falling signal
#define KM_TCC3_EXT_T3_FAL	(_BV(CS32) |_BV(CS31)				)
/// Clock select of Timer3 - Enabled on rising signal
#define KM_TCC3_EXT_T3_RIS	(_BV(CS32) |_BV(CS31) |	_BV(CS30)	)
/// Prescaler mask for Timer3 (allows to clear prescaler)
#define KM_TCC3_CS_MASK		KM_TCC3_EXT_T3_RIS

/// Timer3 mode 0 (Reg.A) - Normal top value 0xFFFF
#define KM_TCC3_MODE_0_A	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 0 (Reg.B)
#define KM_TCC3_MODE_0_B	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 1 (Reg.A) - PWM, Phase Correct, 8-bit top value 0x00FF
#define KM_TCC3_MODE_1_A	(												_BV(WGM30)	)
/// Timer3 mode 1 (Reg.B)
#define KM_TCC3_MODE_1_B	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 2 (Reg.A) - PWM, Phase Correct, 9-bit top value 0x01FF
#define KM_TCC3_MODE_2_A	(								_BV(WGM31)					)
/// Timer3 mode 2 (Reg.B)
#define KM_TCC3_MODE_2_B	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 3 (Reg.A) - PWM, Phase Correct, 10-bit top value 0x03FF
#define KM_TCC3_MODE_3_A	(								_BV(WGM31) |	_BV(WGM30)	)
/// Timer3 mode 3 (Reg.B)
#define KM_TCC3_MODE_3_B	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 4 (Reg.A) - CTC with OCR3A as top
#define KM_TCC3_MODE_4_A	(KMC_ALL_BITS_CLEARED										)
/// Timer3 mode 4 (Reg.B)
#define KM_TCC3_MODE_4_B	(				_BV(WGM32)									)
/// Timer3 mode 5 (Reg.A) - Fast PWM, 8-bit top value 0x00FF
#define KM_TCC3_MODE_5_A	(												_BV(WGM30)	)
/// Timer3 mode 5 (Reg.B)
#define KM_TCC3_MODE_5_B	(				_BV(WGM32)									)
/// Timer3 mode 6 (Reg.A) - Fast PWM, 9-bit top value 0x01FF
#define KM_TCC3_MODE_6_A	(								_BV(WGM31)					)
/// Timer3 mode 5 (Reg.B)
#define KM_TCC3_MODE_6_B	(				_BV(WGM32)									)
/// Timer3 mode 7 (Reg.A) - Fast PWM, 10-bit top value 0x03FF
#define KM_TCC3_MODE_7_A	(								_BV(WGM31) |	_BV(WGM30)	)
/// Timer3 mode 7 (Reg.B)
#define KM_TCC3_MODE_7_B	(				_BV(WGM32)									)
/// Timer3 mode 8 (Reg.A) - PWM, Phase and Frequency Correct with ICR3 as top
#define KM_TCC3_MODE_8_A	(KMC_ALL_BITS_CLEARED									)
/// Timer3 mode 8 (Reg.B)
#define KM_TCC3_MODE_8_B	(_BV(WGM33)													)
/// Timer3 mode 9 (Reg.A) - PWM, Phase and Frequency Correct with OCR3A as top
#define KM_TCC3_MODE_9_A	(												_BV(WGM30)	)
/// Timer3 mode 9 (Reg.B)
#define KM_TCC3_MODE_9_B	(_BV(WGM33)													)
/// Timer3 mode 10 (Reg.A) - PWM, Phase Correct with ICR3 as top
#define KM_TCC3_MODE_A_A	(								_BV(WGM31)					)
/// Timer3 mode 10 (Reg.B)
#define KM_TCC3_MODE_A_B	(_BV(WGM33)													)
/// Timer3 mode 11 (Reg.A) - PWM, Phase Correct with OCR3A as top
#define KM_TCC3_MODE_B_A	(								_BV(WGM31) |	_BV(WGM30)	)
/// Timer3 mode 11 (Reg.B)
#define KM_TCC3_MODE_B_B	(_BV(WGM33)													)
/// Timer3 mode 12 (Reg.A) - CTC with ICR3 as top
#define KM_TCC3_MODE_C_A	(KMC_ALL_BITS_CLEARED									)
/// Timer3 mode 12 (Reg.B)
#define KM_TCC3_MODE_C_B	(_BV(WGM33) |	_BV(WGM32)									)
/// Timer3 mode 13 (Reg.A) - Reserved
#define KM_TCC3_MODE_D_A	(												_BV(WGM30)	)
/// Timer3 mode 13 (Reg.B)
#define KM_TCC3_MODE_D_B	(_BV(WGM33) |	_BV(WGM32)									)
/// Timer3 mode 14 (Reg.A) - Fast PWM with ICR3 as top
#define KM_TCC3_MODE_E_A	(								_BV(WGM31)					)
/// Timer3 mode 14 (Reg.B)
#define KM_TCC3_MODE_E_B	(_BV(WGM33) |	_BV(WGM32)									)
/// Timer3 mode 15 (Reg.A) - Fast PWM with OCR3A as top
#define KM_TCC3_MODE_F_A	(								_BV(WGM31) |	_BV(WGM30)	)
/// Timer3 mode 15 (Reg.B)
#define KM_TCC3_MODE_F_B	(_BV(WGM33) |	_BV(WGM32)									)
/// Mode  mask for Timer3 (reg. A) (helps to clear mode)
#define KM_TCC3_MODE_MASK_A	KM_TCC3_MODE_F_A
/// Mode  mask for Timer3 (reg. B)
#define KM_TCC3_MODE_MASK_B	KM_TCC3_MODE_F_B

/// Normal port operation for compare match
#define KM_TCC3_A_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1A on compare match
#define KM_TCC3_A_COMP_OUT_TOGGLE	(				_BV(COM3A0)	)
/// Clear OC1A on compare match (set output to low level)
#define KM_TCC3_A_COMP_OUT_CLEAR	(_BV(COM3A1)				)
/// Set OC1A on compare match (set output to high level)
#define KM_TCC3_A_COMP_OUT_SET		(_BV(COM3A1) |	_BV(COM3A0)	)
#define KM_TCC3_COMP_A_MASK			KM_TCC3_A_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC3_B_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC3_B_COMP_OUT_TOGGLE	(				_BV(COM3B0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC3_B_COMP_OUT_CLEAR	(_BV(COM3B1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC3_B_COMP_OUT_SET		(_BV(COM3B1) |	_BV(COM3B0)	)
#define KM_TCC3_COMP_B_MASK			KM_TCC3_B_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC3_C_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC3_C_COMP_OUT_TOGGLE	(				_BV(COM3C0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC3_C_COMP_OUT_CLEAR	(_BV(COM3C1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC3_C_COMP_OUT_SET		(_BV(COM3C1) |	_BV(COM3C0)	)
#define KM_TCC3_COMP_C_MASK			KM_TCC3_C_COMP_OUT_SET

/// Normal port operation for compare match, OC3A disconnected
#define KM_TCC3_A_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3A Disconnected @n
/// WGM02 = 1: Toggle OC3A on Compare Match
#define KM_TCC3_A_PWM_COMP_OUT_TOGGLE				(				_BV(COM3A0)	)
/// Fast PWM: Clear OC3A on Compare Match, set OC3A at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3A on Compare Match when up-counting. Set OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC3_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM3A1)				)
/// Fast PWM: Set OC3A on Compare Match, clear OC3A at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC0A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC3_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM3A1) |	_BV(COM3A0)	)

/// Normal port operation for compare match, OC3B disconnected
#define KM_TCC3_B_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3B Disconnected @n
/// WGM02 = 1: Toggle OC3B on Compare Match
#define KM_TCC3_B_PWM_COMP_OUT_TOGGLE				(				_BV(COM3B0)	)
/// Fast PWM: Clear OC3B on Compare Match, set OC3B at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3B on Compare Match when up-counting. Set OC3B on @n
/// Compare Match when down-counting.
#define KM_TCC3_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM3B1)				)
/// Fast PWM: Set OC3B on Compare Match, clear OC3B at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC3_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM3B1) |	_BV(COM3B0)	)

/// Normal port operation for compare match, OC3Cdisconnected
#define KM_TCC3_C_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3C Disconnected @n
/// WGM02 = 1: Toggle OC3C on Compare Match
#define KM_TCC3_C_PWM_COMP_OUT_TOGGLE				(				_BV(COM3C0)	)
/// Fast PWM: Clear OC3C on Compare Match, set OC3c at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3C on Compare Match when up-counting. Set OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC3_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM3C1)				)
/// Fast PWM: Set OC3C on Compare Match, clear OC3C at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3C on Compare Match when up-counting. Clear OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC3_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM3C1) |	_BV(COM3C0)	)

#define KM_TCC3_INT_MASK_REG TIMSK3

#ifdef KM_CPU_AVR_LAYOUT_28
/// Direction register o Timer3 PWM outputs
#define KM_TCC3_PWM_DDR DDRD
/// Port register of Timer3 PWM output port
#define KM_TCC3_PWM_PORT PORTD
/// PWM pin A of Timer3
#define KM_TCC3_PWM_PIN_A PD0
/// PMW pin B of Timer3
#define KM_TCC3_PWM_PIN_B PD2

/// Timer/Counter3 Input Capture Pin
#define KM_TCC3_ICP3_PIN PE2
/// Timer/Counter3 Input Capture DDR
#define KM_TCC3_ICP3_PORT_DDR DDRE
/// Timer/Counter3 Input Capture output port
#define KM_TCC3_ICP3_PORT_OUTPUT PORTE
/// Timer/Counter3 Input Capture input port
#define KM_TCC3_ICP3_PORT_INPUT PINE

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC3_T3_PIN PE3
#endif /* KM_CPU_AVR_LAYOUT_100 */

#ifdef KM_CPU_AVR_LAYOUT_100
/// Direction register o Timer3 PWM outputs
#define KM_TCC3_PWM_DDR DDRE
/// Port register of Timer3 PWM output port
#define KM_TCC3_PWM_PORT PORTE
/// PWM pin A of Timer3
#define KM_TCC3_PWM_PIN_A PE3
/// PMW pin B of Timer3
#define KM_TCC3_PWM_PIN_B PE4
/// PMW pin C of Timer3
#define KM_TCC3_PWM_PIN_C PE5

/// Timer/Counter3 Input Capture Pin
#define KM_TCC3_ICP3_PIN PE7
/// Timer/Counter3 Input Capture DDR
#define KM_TCC3_ICP3_PORT_DDR DDRE
/// Timer/Counter3 Input Capture output port
#define KM_TCC3_ICP3_PORT_OUTPUT PORTE
/// Timer/Counter3 Input Capture input port
#define KM_TCC3_ICP3_PORT_INPUT PINE

/// Timer/Counter5 Input Clock input Pin
#define KM_TCC3_T3_PIN PE6
#endif /* KM_CPU_AVR_LAYOUT_100 */

/// Byte value of PWM pin A of Timer3
#define KM_TCC3_PWM_BV_A	_BV(KM_TCC3_PWM_PIN_A)
/// Byte value of PWM pin B of Timer3
#define KM_TCC3_PWM_BV_B	_BV(KM_TCC3_PWM_PIN_B)
/// Byte value of PWM pin C of Timer3
#define KM_TCC3_PWM_BV_C	_BV(KM_TCC3_PWM_PIN_C)

#endif /* TIMSK3 */
#endif
#ifdef __cplusplus
}

#endif /* KM_TIMER_3_DEFS_H_ */
