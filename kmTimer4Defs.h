/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* kmTimer4Defs.h
*
*  **Created on**: 12/25/2023 7:14:34 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2023 Krzysztof Moskwa
*  kmTimer4Defs.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KM_TIMER_4_DEFS_H_
#define KM_TIMER_4_DEFS_H_

#ifdef KM_DOXYGEN
#define TIMSK4
#define COM4C0
#define KM_CPU_AVR_LAYOUT_100
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef TIMSK4

#include "../kmCommon/kmCommon.h"
#include "../kmCpu/kmCpuDefs.h"
#include "kmTimerDefs.h"

/// Defines bottom value for Timer4
#define KM_TIMER4_BOTTOM KM_TCC_BOTOM
/// Defines top value for Timer4
#define KM_TIMER4_MAX KM_TCC_TOP_3
#define KM_TIMER4_TOP_8_BIT KM_TCC_TOP_1
#define KM_TIMER4_TOP_9_BIT KM_TCC_TOP_5
#define KM_TIMER4_TOP_10_BIT KM_TCC_TOP_4
#define KM_TIMER4_TOP_16_BIT KM_TCC_TOP_3
/// Defines mid value for Timer4
#define KM_TIMER4_MID KM_TCC_MID_3

/// Clock select of Timer4 - Disabled
#define KM_TCC4_STOP		(KMC_ALL_BITS_CLEARED				)
/// Clock select of Timer4 - Prescaler 1 / 1
#define KM_TCC4_PRSC_1		(						_BV(CS40)	)
/// Clock select of Timer4 - Prescaler 1 / 8
#define KM_TCC4_PRSC_8		(			_BV(CS41)				)
/// Clock select of Timer4 - Prescaler 1 / 64
#define KM_TCC4_PRSC_64	(				_BV(CS41) |	_BV(CS40)	)
/// Clock select of Timer4 - Prescaler 1 / 256
#define KM_TCC4_PRSC_256	(_BV(CS42)							)
/// Clock select of Timer4 - Prescaler 1 / 1024
#define KM_TCC4_PRSC_1024	(_BV(CS42) |			_BV(CS40)	)
/// Clock select of Timer4 - Enabled on falling signal
#define KM_TCC4_EXT_T4_FAL	(_BV(CS42) |_BV(CS41)				)
/// Clock select of Timer4 - Enabled on rising signal
#define KM_TCC4_EXT_T4_RIS	(_BV(CS42) |_BV(CS41) |	_BV(CS40)	)
/// Prescaler mask for Timer4 (allows to clear prescaler)
#define KM_TCC4_CS_MASK		KM_TCC4_EXT_T4_RIS

/// Timer4 mode 0 (Reg.A) - Normal top value 0xFFFF
#define KM_TCC4_MODE_0_A	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 0 (Reg.B)
#define KM_TCC4_MODE_0_B	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 1 (Reg.A) - PWM, Phase Correct, 8-bit top value 0x00FF
#define KM_TCC4_MODE_1_A	(												_BV(WGM40)	)
/// Timer4 mode 1 (Reg.B)
#define KM_TCC4_MODE_1_B	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 2 (Reg.A) - PWM, Phase Correct, 9-bit top value 0x01FF
#define KM_TCC4_MODE_2_A	(								_BV(WGM41)					)
/// Timer4 mode 2 (Reg.B)
#define KM_TCC4_MODE_2_B	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 3 (Reg.A) - PWM, Phase Correct, 10-bit top value 0x03FF
#define KM_TCC4_MODE_3_A	(								_BV(WGM41) |	_BV(WGM40)	)
/// Timer4 mode 3 (Reg.B)
#define KM_TCC4_MODE_3_B	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 4 (Reg.A) - CTC with OCR3A as top
#define KM_TCC4_MODE_4_A	(KMC_ALL_BITS_CLEARED										)
/// Timer4 mode 4 (Reg.B)
#define KM_TCC4_MODE_4_B	(				_BV(WGM42)									)
/// Timer4 mode 5 (Reg.A) - Fast PWM, 8-bit top value 0x00FF
#define KM_TCC4_MODE_5_A	(												_BV(WGM40)	)
/// Timer4 mode 5 (Reg.B)
#define KM_TCC4_MODE_5_B	(				_BV(WGM42)									)
/// Timer4 mode 6 (Reg.A) - Fast PWM, 9-bit top value 0x01FF
#define KM_TCC4_MODE_6_A	(								_BV(WGM41)					)
/// Timer4 mode 5 (Reg.B)
#define KM_TCC4_MODE_6_B	(				_BV(WGM42)									)
/// Timer4 mode 7 (Reg.A) - Fast PWM, 10-bit top value 0x03FF
#define KM_TCC4_MODE_7_A	(								_BV(WGM41) |	_BV(WGM40)	)
/// Timer4 mode 7 (Reg.B)
#define KM_TCC4_MODE_7_B	(				_BV(WGM42)									)
/// Timer4 mode 8 (Reg.A) - PWM, Phase and Frequency Correct with ICR3 as top
#define KM_TCC4_MODE_8_A	(KMC_ALL_BITS_CLEARED									)
/// Timer4 mode 8 (Reg.B)
#define KM_TCC4_MODE_8_B	(_BV(WGM43)													)
/// Timer4 mode 9 (Reg.A) - PWM, Phase and Frequency Correct with OCR3A as top
#define KM_TCC4_MODE_9_A	(												_BV(WGM40)	)
/// Timer4 mode 9 (Reg.B)
#define KM_TCC4_MODE_9_B	(_BV(WGM43)													)
/// Timer4 mode 10 (Reg.A) - PWM, Phase Correct with ICR3 as top
#define KM_TCC4_MODE_A_A	(								_BV(WGM41)					)
/// Timer4 mode 10 (Reg.B)
#define KM_TCC4_MODE_A_B	(_BV(WGM43)													)
/// Timer4 mode 11 (Reg.A) - PWM, Phase Correct with OCR3A as top
#define KM_TCC4_MODE_B_A	(								_BV(WGM41) |	_BV(WGM40)	)
/// Timer4 mode 11 (Reg.B)
#define KM_TCC4_MODE_B_B	(_BV(WGM43)													)
/// Timer4 mode 12 (Reg.A) - CTC with ICR3 as top
#define KM_TCC4_MODE_C_A	(KMC_ALL_BITS_CLEARED									)
/// Timer4 mode 12 (Reg.B)
#define KM_TCC4_MODE_C_B	(_BV(WGM43) |	_BV(WGM42)									)
/// Timer4 mode 13 (Reg.A) - Reserved
#define KM_TCC4_MODE_D_A	(												_BV(WGM40)	)
/// Timer4 mode 13 (Reg.B)
#define KM_TCC4_MODE_D_B	(_BV(WGM43) |	_BV(WGM42)									)
/// Timer4 mode 14 (Reg.A) - Fast PWM with ICR3 as top
#define KM_TCC4_MODE_E_A	(								_BV(WGM41)					)
/// Timer4 mode 14 (Reg.B)
#define KM_TCC4_MODE_E_B	(_BV(WGM43) |	_BV(WGM42)									)
/// Timer4 mode 15 (Reg.A) - Fast PWM with OCR3A as top
#define KM_TCC4_MODE_F_A	(								_BV(WGM41) |	_BV(WGM40)	)
/// Timer4 mode 15 (Reg.B)
#define KM_TCC4_MODE_F_B	(_BV(WGM43) |	_BV(WGM42)									)
/// Mode  mask for Timer4 (reg. A) (helps to clear mode)
#define KM_TCC4_MODE_MASK_A	KM_TCC4_MODE_F_A
/// Mode  mask for Timer4 (reg. B)
#define KM_TCC4_MODE_MASK_B	KM_TCC4_MODE_F_B

/// Normal port operation for compare match
#define KM_TCC4_A_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1A on compare match
#define KM_TCC4_A_COMP_OUT_TOGGLE	(				_BV(COM4A0)	)
/// Clear OC1A on compare match (set output to low level)
#define KM_TCC4_A_COMP_OUT_CLEAR	(_BV(COM4A1)				)
/// Set OC1A on compare match (set output to high level)
#define KM_TCC4_A_COMP_OUT_SET		(_BV(COM4A1) |	_BV(COM4A0)	)
#define KM_TCC4_COMP_A_MASK			KM_TCC4_A_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC4_B_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC4_B_COMP_OUT_TOGGLE	(				_BV(COM4B0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC4_B_COMP_OUT_CLEAR	(_BV(COM4B1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC4_B_COMP_OUT_SET		(_BV(COM4B1) |	_BV(COM4B0)	)
#define KM_TCC4_COMP_B_MASK			KM_TCC4_B_COMP_OUT_SET

/// Normal port operation for compare match
#define KM_TCC4_C_COMP_OUT_NONE		(KMC_ALL_BITS_CLEARED		)
/// Toggle OC1B on compare match
#define KM_TCC4_C_COMP_OUT_TOGGLE	(				_BV(COM4C0)	)
/// Clear OC1B on compare match (set output to low level)
#define KM_TCC4_C_COMP_OUT_CLEAR	(_BV(COM4C1)				)
/// Set OC1B on compare match (set output to high level)
#define KM_TCC4_C_COMP_OUT_SET		(_BV(COM4C1) |	_BV(COM4C0)	)
#define KM_TCC4_COMP_C_MASK			KM_TCC4_C_COMP_OUT_SET

/// Normal port operation for compare match, OC3A disconnected
#define KM_TCC4_A_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3A Disconnected @n
/// WGM02 = 1: Toggle OC3A on Compare Match
#define KM_TCC4_A_PWM_COMP_OUT_TOGGLE				(				_BV(COM4A0)	)
/// Fast PWM: Clear OC3A on Compare Match, set OC3A at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3A on Compare Match when up-counting. Set OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC4_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM4A1)				)
/// Fast PWM: Set OC3A on Compare Match, clear OC3A at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC0A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC4_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM4A1) |	_BV(COM4A0)	)

/// Normal port operation for compare match, OC3B disconnected
#define KM_TCC4_B_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3B Disconnected @n
/// WGM02 = 1: Toggle OC3B on Compare Match
#define KM_TCC4_B_PWM_COMP_OUT_TOGGLE				(				_BV(COM4B0)	)
/// Fast PWM: Clear OC3B on Compare Match, set OC3B at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3B on Compare Match when up-counting. Set OC3B on @n
/// Compare Match when down-counting.
#define KM_TCC4_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM4B1)				)
/// Fast PWM: Set OC3B on Compare Match, clear OC3B at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3A on Compare Match when up-counting. Clear OC3A on @n
/// Compare Match when down-counting.
#define KM_TCC4_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM4B1) |	_BV(COM4B0)	)

/// Normal port operation for compare match, OC3Cdisconnected
#define KM_TCC4_C_PWM_COMP_OUT_NORMAL				(KMC_ALL_BITS_CLEARED		)
/// Fast PWM / Phase Accurate PWM @n
/// WGM02 = 0: Normal Port Operation, OC3C Disconnected @n
/// WGM02 = 1: Toggle OC3C on Compare Match
#define KM_TCC4_C_PWM_COMP_OUT_TOGGLE				(				_BV(COM4C0)	)
/// Fast PWM: Clear OC3C on Compare Match, set OC3c at BOTTOM (non-inverting mode) @n
/// Phase Accurate PWM: Clear OC3C on Compare Match when up-counting. Set OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC4_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN	(_BV(COM4C1)				)
#ifdef COM4C0
/// Fast PWM: Set OC3C on Compare Match, clear OC3C at BOTTOM (inverting mode) @n
/// Phase Accurate PWM: Set OC3C on Compare Match when up-counting. Clear OC3C on @n
/// Compare Match when down-counting.
#define KM_TCC4_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN	(_BV(COM4C1) |	_BV(COM4C0)	)
#endif

#define KM_KM_TCC4_INT_MASK_REG TIMSK4

#ifdef KM_CPU_AVR_LAYOUT_28
/// Direction register o Timer4 PWM outputs
#define KM_TCC4_PWM_DDR DDRD
/// Port register of Timer4 PWM outputs
#define KM_TCC4_PWM_PORT PORTD
/// PWM pin A of Timer4
#define KM_TCC4_PWM_PIN_A PD1
/// PMW pin B of Timer4
#define KM_TCC4_PWM_PIN_B PD2

/// Timer/Counter4 Input Capture Pin
#define KM_TCC4_ICP4_PORT_DDR DDRE
#define KM_TCC4_ICP4_PORT_OUTPUT PORTE
#define KM_TCC4_ICP4_PORT_INPUT PINE
#define KM_TCC4_ICP4_PIN PE0
///
#define KM_TCC4_T4_PIN PE1
#endif /* KM_CPU_AVR_LAYOUT_28 */

#ifdef KM_CPU_AVR_LAYOUT_100
/// Direction register o Timer4 PWM outputs
#define KM_TCC4_PWM_DDR DDRH
/// Port register of Timer4 PWM outputs
#define KM_TCC4_PWM_PORT PORTH
/// PWM pin A of Timer4
#define KM_TCC4_PWM_PIN_A PH3
/// PMW pin B of Timer4
#define KM_TCC4_PWM_PIN_B PH4
/// PMW pin C of Timer4
#define KM_TCC4_PWM_PIN_C PH5

/// Timer/Counter5 Input Capture Pin
#define KM_TCC4_ICP4_PIN PL0
/// Timer/Counter5 Input Capture DDR
#define KM_TCC4_ICP4_PORT_DDR DDRL
/// Timer/Counter5 Input Capture output port
#define KM_TCC4_ICP4_PORT_OUTPUT PORTL
/// Timer/Counter5 Input Capture input port
#define KM_TCC4_ICP4_PORT_INPUT PORTL

/// Timer/Counter4 Input Clock input Pin
#define KM_TCC4_T4_PIN PH7
#endif /* KM_CPU_AVR_LAYOUT_100 */

/// Byte value of PWM pin A of Timer4
#define KM_TCC4_PWM_BV_A	_BV(KM_TCC4_PWM_PIN_A)
/// Byte value of PWM pin B of Timer4
#define KM_TCC4_PWM_BV_B	_BV(KM_TCC4_PWM_PIN_B)
/// Byte value of PWM pin C of Timer4
#define KM_TCC4_PWM_BV_C	_BV(KM_TCC4_PWM_PIN_C)

#endif /* TIMSK4 */

#ifdef __cplusplus
}
#endif
#endif /* KM_TIMER_4_DEFS_H_ */
